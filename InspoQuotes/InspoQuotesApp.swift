//
//  InspoQuotesApp.swift
//  InspoQuotes
//
//  Created by Isizwe Madalane on 2021/07/03.
//

import SwiftUI

@main
struct InspoQuotesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
