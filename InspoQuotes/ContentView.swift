//
//  ContentView.swift
//  InspoQuotes
//
//  Created by Isizwe Madalane on 2021/07/03.
//
import StoreKit
import SwiftUI

struct ContentView: View {
    let productID = "com.exampleisizwe.InspoQuotes"
    var quotesToShow = [
        Quote(quoteToShow: "Our greatest glory is not in never falling, but in rising every time we fall. — Confucius"),
        Quote(quoteToShow: "All our dreams can come true, if we have the courage to pursue them. – Walt Disney"),
        Quote(quoteToShow: "It does not matter how slowly you go as long as you do not stop. – Confucius"),
        Quote(quoteToShow: "Everything you’ve ever wanted is on the other side of fear. — George Addair"),
        Quote(quoteToShow: "Success is not final, failure is not fatal: it is the courage to continue that counts. – Winston Churchill"),
        Quote(quoteToShow: "Hardships often prepare ordinary people for an extraordinary destiny. – C.S. Lewis")
    ]
    
    let premiumQuotes = [
        Quote(quoteToShow: "Believe in yourself. You are braver than you think, more talented than you know, and capable of more than you imagine. ― Roy T. Bennett"),
        Quote(quoteToShow: "I learned that courage was not the absence of fear, but the triumph over it. The brave man is not he who does not feel afraid, but he who conquers that fear. – Nelson Mandela"),
        Quote(quoteToShow: "There is only one thing that makes a dream impossible to achieve: the fear of failure. ― Paulo Coelho"),
        Quote(quoteToShow: "It’s not whether you get knocked down. It’s whether you get up. – Vince Lombardi"),
        Quote(quoteToShow: "Your true success in life begins only when you make the commitment to become excellent at what you do. — Brian Tracy"),
        Quote(quoteToShow: "Believe in yourself, take on your challenges, dig deep within yourself to conquer fears. Never let anyone bring you down. You got to keep going. – Chantal Sutherland")
    ]
    
    var body: some View {
        NavigationView {
            VStack {
                List(quotesToShow) { quote in
                    Text(quote.quoteToShow)
                }
                Button(action: {
                    buyPremiumQuotes()
                }, label: {
                    Text("Get More Quotes")
                }).padding(.bottom, 200)
            }
            .navigationTitle("InspoQuotes")
            .navigationBarItems(trailing: Button(action: {
                print("Restore button pressed")
            }, label: {
                Text("Restore")
            }))
        }
        /**
        .onAppear {
         SKPaymentQueue.default().add(self)
         
        }
         */
    }
    
    func buyPremiumQuotes() {
        if SKPaymentQueue.canMakePayments() {
            /**
             Requires the class to inherit from SKPaymentTransactionObserver so the following will not work at the moment
             */
            // Can make payments
            let paymentRequest = SKMutablePayment()
            paymentRequest.productIdentifier = productID
            SKPaymentQueue.default().add(paymentRequest)
        } else {
            // Can't make payments
            print("User can't make payments")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct Quote: Identifiable {
    var id: String {
        return quoteToShow
    }
    var quoteToShow: String
}
